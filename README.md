# GitLab CI Templates

This is a list of my commonly used CI configurations for GitLab

## Conventions

- Named versions

## Requirements for Templates

### Required

Unless by exception:

- Linting - Ensure it meets coding standards
- Static Tests - Run static tests against the code
- Build - Compile the program
- Unit Tests - Run code tests
- Vulnerability Scanning - Run a vulnerability and dependency scanner like Snyk, or requires.io
- Package - Make it deployable
- Deploy - Publish the package to common locations

### Expected or in some cases

- Alex language neutrality (still to come)
- Spell checking (still to come)

## Environments

### Python

### Docker

### Terraform

# License

AGPLv3
