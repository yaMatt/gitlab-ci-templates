# Variables

- _PACKAGE_NAME_ - The name of your Docker image.
- _REGISTRY_ - URL for the image registry. Use the default `docker.io`
- _REGISTRY_USER_ - The username you use for the image registry
- _REGISTRY_PASSWORD_ - The password for the user
